import os as OS
import shutil

print("###############################################################################")
print("#          SottoInsieme del contenuto di una directory                        #")
print("###############################################################################")
dir_srt=input("Inserisci la Directory da cui vuoi creare un sottoinsieme:")
file_txt=input("Inserisci il file di testo in formato .txt con i nomi degli elementi da filtrare:")
dir_out=input("Inserisci la Directory in cui creare il sottoinsieme:")
if dir_srt is None or dir_srt == "":
    print("Errore: Directory di partenza non inserita")
    exit()
else:
    if not OS.path.exists(dir_srt):
     print("Errore: la directory del sottoinsieme non esiste!, si prega di controllare i dati inseriti e di riprovare")
     exit()
    else:
       if OS.path.getsize(dir_srt) == 0:
          print("Il file inserito è vuoto!")
          exit()
if file_txt is None or file_txt == "":
    print("Errore: file con gli elementi di filtraggio non inserito")
    exit()
else:
    if not file_txt.endswith(".txt"):
      print("Formato del file sconosciuto")
      exit()
    else:
       if not OS.path.isfile(file_txt):
        print("Errore: il file con gli elementi di filtraggio non esiste!, si prega di controllare i dati inseriti e di riprovare")
        exit()
       else:
        if OS.path.getsize(file_txt) == 0:
            print("Il file inserito è vuoto!")
            exit()
if dir_out is None or dir_out == "":
    print("Errore: Directory del sottoinsieme non inserita")
    exit()
else:
    if not OS.path.exists(dir_out):
     print("creo la directory "+dir_out)
     OS.makedirs(dir_out)
print("Sto per creare il sotto insieme partendo da questi parametri: ")
print("DirPartenza: "+dir_srt)
print("il file elementi sottoinsieme: "+file_txt)
print("DirSottoinsieme: "+dir_out)
file_txt1=OS.path.basename(file_txt)
list_input=OS.listdir(dir_srt)
with open(file_txt1, "r") as file:
  for line in file:
    print(line)
    if line.replace("\n","") in list_input:
      dir1=OS.path.join(dir_srt,line.replace("\n",""))
      dir2=OS.path.join(dir_out,line.replace("\n",""))
      print(dir1)
      print(dir2)
      shutil.copy2(dir1,dir2)
print("creato un sottoinsieme della directory: "+dir_srt+" di lunghezza ",len(OS.listdir(dir_srt)))
print("il sottoinsieme  "+dir_out+" di lunghezza ",len(OS.listdir(dir_out)))
file.close()